import json

from django.http import HttpResponse, JsonResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate
from django import forms
from base64 import b64decode

from .models import Item, Review

# form included into views.py according to task grader requirements (see readme)
class AddItemForm(forms.Form):
    title = forms.CharField(max_length=64, required=True)
    description = forms.CharField(max_length=1024, required=True)
    price = forms.IntegerField(min_value=1, max_value=1000000, required=True)

    def clean(self):
        cleaned_data = super().clean()
        if type(self.data.get('title')) != str or type(self.data.get('description')) != str:
            raise forms.ValidationError('Title must be str')
        else:
            return cleaned_data


class AddReviewForm(forms.Form):
    text = forms.CharField(max_length=1024, required=True)
    grade = forms.IntegerField(min_value=1, max_value=10, required=True)

    def clean(self):
        cleaned_data = super().clean()
        if type(self.data.get('text')) != str:
            raise forms.ValidationError('Text must be str')
        else:
            return cleaned_data


@method_decorator(csrf_exempt, name='dispatch')
class AddItemView(View):

    def post(self, request):
        encoded_creds = request.META.get('HTTP_AUTHORIZATION', None)
        if encoded_creds is None:
            return HttpResponse(status=401)
        creds_pair = b64decode(encoded_creds.split()[1]).decode()
        username, passwd = creds_pair.split(':')
        user = authenticate(username=username, password=passwd)
        if user:
            if user.is_staff:
                try:
                    form = AddItemForm(json.loads(request.body))
                    if form.is_valid():
                        new_item = Item(title=form.cleaned_data['title'],
                                        description=form.cleaned_data['description'],
                                        price=form.cleaned_data['price'])
                        new_item.save()
                        return JsonResponse({'id': new_item.id}, status=201, safe=False)
                    else:
                        return HttpResponse(status=400)
                except json.JSONDecodeError:
                    return HttpResponse(status=400)
            else:
                return HttpResponse(status=403)
        else:
            return HttpResponse(status=401)


@method_decorator(csrf_exempt, name='dispatch')
class PostReviewView(View):

    def post(self, request, item_id):
        try:
            form = AddReviewForm(json.loads(request.body))
            if form.is_valid():
                if Item.objects.filter(pk=item_id).exists():
                    review = Review(text=form.cleaned_data['text'],
                                    grade=form.cleaned_data['grade'],
                                    item_id=item_id)
                    review.save()
                    return JsonResponse({'id': review.id}, status=201)
                else:
                    return HttpResponse(status=404)
            else:
                return HttpResponse(status=400)
        except json.JSONDecodeError:
            return HttpResponse(status=400)


@method_decorator(csrf_exempt, name='dispatch')
class GetItemView(View):

    def get(self, request, item_id):
        if Item.objects.filter(pk=item_id).exists():
            item_data = Item.objects.filter(pk=item_id)[0]
            item_reviews = Review.objects.filter(item=item_id).order_by('-id')[:5]
            reviews = []
            for review in item_reviews:
                reviews.append({
                    'id': review.id,
                    'text': review.text,
                    'grade': review.grade
                })
            result = {
                'id': item_id,
                'title': item_data.title,
                'description': item_data.description,
                'price': item_data.price,
                'reviews': reviews
            }
            return JsonResponse(result, status=200)
        else:
            return HttpResponse(status=404)
